1) A quoi servent les commandes suivantes sur un terminal bash

pwd : pour savoir où on se trouve 
cd : pour se déplacer
cp : pour copier
ls : pour connaitre les fichiers dans le dossier courant	
ls -la : 
mkdir : pour créer un répertoire	
rm : pour supprimer un fichier 
rm -rf : pour supprimer un répertoire
nano fichier : pour utiliser l'éditeur de test

2) A quoi servent les commandes suivantes sur git

git init : créer un espace vide ou réinitialise
git add fichier : sert à indexer
git commit -m "add fichier" : valide une modification et ajoute un nom à cette version
git push : sert à actuliser
git clone : sert à créer une copie d'un espace de stockage
git diff : pour analyser l'état actuel d'un espace git
git rm : sert à sûpprimer des fichier d'un espace git
git log --oneline : sert à récupérer l'historique de version avec les id
git config --global alias.tree 'log --graph --full-history --all --color --date=short --pretty=format:"%Cred%x09%h %Creset%ad%Cblue%d %Creset %s %C(bold)(%an)%Creset"' : 

3) URL de votre dépôt GIT en https :
https://gitlab.com/ludovic_berthuy/toto_formation.git
4) Déposez vos réponses sur la plateforme AJC Classroom ET sur votre projet git
